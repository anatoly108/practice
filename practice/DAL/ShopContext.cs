﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using practice.Models;

namespace practice.DAL
{
    public class ShopContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Element> Elements { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Order> Orders { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<Order>().HasRequired(x => x.Customer);
        }
    }
}