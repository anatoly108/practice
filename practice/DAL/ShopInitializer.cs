﻿using System.Collections.Generic;
using System.Data.Entity;
using practice.Models;

namespace practice.DAL
{
    public class ShopInitializer : DropCreateDatabaseIfModelChanges<ShopContext>
    {
        protected override void Seed(ShopContext context)
        {
            var items = new List<Item>
            {
                new Item()
                {
                    Name = "Товар"
                }
            };
            items.ForEach(x => context.Items.Add(x));
            context.SaveChanges();

            var customers = new List<Customer>
            {
                new Customer()
                {
                    Name = "Покупатель"
                }
            };

            customers.ForEach(x => context.Customers.Add(x));
            context.SaveChanges();

        }
    }
}