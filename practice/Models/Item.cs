﻿namespace practice.Models
{
    public class Item
    {
        public int ID { get; set; } 
        public string Code { get; set; } 
        public string Name { get; set; } 
        public int Price { get; set; } 
        public string Category { get; set; } 

    }
}