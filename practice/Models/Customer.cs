﻿namespace practice.Models
{
    public class Customer
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Address { get; set; }
        public string Discount { get; set; }
    }
}