﻿using System;
using System.ComponentModel.DataAnnotations;

namespace practice.Models
{
    public class Order
    {
        public int ID { get; set; } 
        public virtual Customer Customer { get; set; } 
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)] 
        public DateTime OrderDate { get; set; } 
        public DateTime ShipDate { get; set; } 
        public int Number { get; set; } 
        public string Status { get; set; }
    }
}