﻿namespace practice.Models
{
    public class Element
    {
        public int ID { get; set; } 
        public virtual Order Order { get; set; } 
        public virtual Item Item { get; set; } 
        public int ItemsCount { get; set; } 
        public int ItemPrice { get; set; } 
    }
}