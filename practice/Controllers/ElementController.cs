﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using practice.Models;
using practice.DAL;

namespace practice.Controllers
{
    public class ElementController : Controller
    {
        private ShopContext db = new ShopContext();

        //
        // GET: /Element/

        public ActionResult Index()
        {
            return View(db.Elements.ToList());
        }

        //
        // GET: /Element/Details/5

        public ActionResult Details(int id = 0)
        {
            Element element = db.Elements.Find(id);
            if (element == null)
            {
                return HttpNotFound();
            }
            return View(element);
        }

        //
        // GET: /Element/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Element/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Element element)
        {
            if (ModelState.IsValid)
            {
                db.Elements.Add(element);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(element);
        }

        //
        // GET: /Element/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Element element = db.Elements.Find(id);
            if (element == null)
            {
                return HttpNotFound();
            }
            return View(element);
        }

        //
        // POST: /Element/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Element element)
        {
            if (ModelState.IsValid)
            {
                db.Entry(element).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(element);
        }

        //
        // GET: /Element/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Element element = db.Elements.Find(id);
            if (element == null)
            {
                return HttpNotFound();
            }
            return View(element);
        }

        //
        // POST: /Element/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Element element = db.Elements.Find(id);
            db.Elements.Remove(element);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}